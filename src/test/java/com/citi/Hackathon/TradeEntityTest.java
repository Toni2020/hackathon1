package com.citi.Hackathon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import com.citi.Hackathon.entities.Trade;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TradeEntityTest {
    
    Trade trade = new Trade();

    @Test
    void testSetAndGetId() {
        ObjectId id = new ObjectId();
        trade.setId(id);
        assertEquals(id, trade.getId());
    }

    @Test
    void testSetAndGetTicker() {
        trade.setTicker("TEST");
        assertEquals("TEST", trade.getTicker());
    }

    @Test
    void testSetAndGetAmount() {
        trade.setAmount(123);
        assertEquals(123, trade.getAmount());
    }

    @Test
    void testSetAndGetTradeType() {
        trade.setTradeType("BUY");
        assertEquals("BUY", trade.getTradeType());
    }

    @Test
    void testSetAndGetDateCreated() {
        LocalDateTime date = LocalDateTime.now();
        trade.setDateCreated(date);
        assertEquals(date, trade.getDateCreated());
    }

    @Test
    void testSetAndGetState() {
        trade.setState("TEST");
        assertEquals("TEST", trade.getState());
    }

    @Test
    void testSetAndGetPrice() {
        trade.setPrice(123);
        assertEquals(123, trade.getPrice());
    }

}

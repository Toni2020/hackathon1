package com.citi.Hackathon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Collection;

import com.citi.Hackathon.entities.Trade;
import com.citi.Hackathon.rest.TradeController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TradeControllerTest {
    
    @Autowired
    TradeController controller;

    @Test
    void testGet() {
        Collection<Trade> trades = controller.getTrades();
        assertFalse(trades.isEmpty());
    }

    @Test
    void testPost() {
        Collection<Trade> tradesBefore = controller.getTrades();

        Trade trade = new Trade();
        trade.setTicker("IBM");
        trade.setAmount(1);
        trade.setPrice(135.453);
        trade.setTradeType("Buy");
        controller.addTrade(trade);

        Collection<Trade> tradesAfter = controller.getTrades();
        int sizeDif = tradesAfter.size() - tradesBefore.size();

        assertEquals(1, sizeDif);
    }

}

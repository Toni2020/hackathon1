package com.citi.Hackathon.rest;

import java.util.Collection;

import com.citi.Hackathon.entities.Trade;
import com.citi.Hackathon.service.TradeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/trade")
public class TradeController {
    
    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades() {
        return tradeService.getTrade();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        tradeService.addToPortfolio(trade);
    }
    
}
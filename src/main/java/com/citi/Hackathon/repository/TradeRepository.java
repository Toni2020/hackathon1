package com.citi.Hackathon.repository;

import com.citi.Hackathon.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId>{

    

}
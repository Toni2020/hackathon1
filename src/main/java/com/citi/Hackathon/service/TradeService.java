package com.citi.Hackathon.service;

import java.util.Collection;

import com.citi.Hackathon.entities.Trade;

public interface TradeService {

    void addToPortfolio(Trade trade);
    Collection<Trade> getTrade();
}
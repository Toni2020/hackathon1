package com.citi.Hackathon.service;

import java.util.Collection;

import com.citi.Hackathon.entities.Trade;
import com.citi.Hackathon.repository.TradeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImp implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override
    public void addToPortfolio(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public Collection<Trade> getTrade() {
        return repo.findAll();
    }
    
    
}